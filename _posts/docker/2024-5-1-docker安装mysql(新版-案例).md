---
layout: post
title:  "docker安装mysql(新版-案例)"
date:   2024-05-01 10:00:00
categories: Docker
tags: Docker
excerpt: docker安装mysql
mathjax: true
---

安装mysql5.7



```
docker run -itd --name mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql:5.7.36
```

